{% extends 'base.tpl' -%}

{% block gui_fkey1 %}{% endblock %}

{%- block dnd_param %}
    <dkey_dnd perm="R">keyevent none</dkey_dnd>
    <gui_fkey4 perm="R">none</gui_fkey4>
{% endblock -%}

{% block settings_suffix %}
    <gui_fkey1 perm="R">F_ADR_BOOK</gui_fkey1>
{% endblock %}
